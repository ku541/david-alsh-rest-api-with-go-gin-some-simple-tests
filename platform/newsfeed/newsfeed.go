package newsfeed

type Getter interface {
	Get() []Item
}

type Creator interface {
	Create(item Item)
}

type Item struct {
	Title string `json:"title"`
	Post  string `json:"post"`
}

type Repo struct {
	Items []Item
}

func New() *Repo {
	return &Repo{
		Items: []Item{},
	}
}

func (r *Repo) Create(item Item) {
	r.Items = append(r.Items, item)
}

func (r *Repo) Get() []Item {
	return r.Items
}
