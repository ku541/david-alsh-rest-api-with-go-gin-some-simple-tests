package newsfeed

import "testing"

func TestCreate(t *testing.T) {
	feed := New()

	feed.Create(Item{})

	if len(feed.Items) != 1 {
		t.Errorf("Item was not added")
	}
}

func TestIndex(t *testing.T) {
	feed := New()

	feed.Create(Item{})

	if len(feed.Get()) != 1 {
		t.Errorf("Item was not added")
	}
}
